# CamUtils

A collection of core utilities written for fun.

## Philosophy

The UNIX philosophy is "do one thing and do it well". Unfortunately, this is a
very rare concept nowadays. Most program use a "do several things and do them
badly" philosophy. All of my core utils have only one way to function. For
example, my `uniq` command only searches for unique strings (unlike the GNU
version that also looks for duplicates or compares substrings).

My favorite lost concept is reading STDOUT, STDIN, and STDERR for almost
everything. A lot of modern tools inconsistently use a file or stdin when most
shells support reading from a file. These tools (with the exception of `cat` are
designed to use pipes for input/output. I admit this is an odd concept after
working with horrible software. Check the examples below for how this is
supposed to work.

The UNIX way (accepted):

`cat inFile | uniq`

The shell way (accepted):

`uniq < inFile`

The modern way (terrible, unsupported by most utilities):

`uniq inFile outFile`

Of course, there are exceptions where I have to use file paths. I already
mentioned `cat` above. Other commands like `cp`, `rm`, `shred` are intended to
be used with files so I am not changing them in any way.

## Building

Building is very straightforward.

1. Clone the repository:
`git clone https://gitlab.com/caton101/camutils.git`
2. Run the makefile generator:
`./genmakefile.sh`
3. Build the code:
`make all`

## Is it implemented?

Personal Tools

- [ ] `help`
- [X] `scrub`

GNU Core Utils

- [ ] `[`
- [ ] `b2sum`
- [ ] `base32`
- [ ] `base64`
- [ ] `basename`
- [ ] `basenc`
- [X] `cat`
- [ ] `chcon`
- [ ] `chgrp`
- [ ] `chmod`
- [ ] `chown`
- [ ] `chroot`
- [ ] `cksum`
- [ ] `comm`
- [X] `cp`
- [ ] `csplit`
- [ ] `cut`
- [ ] `date`
- [ ] `dd`
- [ ] `df`
- [ ] `dir`
- [ ] `dircolors`
- [ ] `dirname`
- [ ] `du`
- [X] `echo`
- [ ] `env`
- [ ] `expand`
- [ ] `expr`
- [ ] `factor`
- [ ] `false`
- [ ] `fmt`
- [ ] `fold`
- [ ] `head`
- [ ] `hostid`
- [ ] `id`
- [ ] `install`
- [ ] `join`
- [ ] `link`
- [ ] `ln`
- [ ] `logname`
- [ ] `ls`
- [ ] `md5sum`
- [ ] `mkdir`
- [ ] `mkfifo`
- [ ] `mknod`
- [ ] `mktemp`
- [ ] `mv`
- [ ] `nice`
- [ ] `nl`
- [ ] `nohup`
- [ ] `nproc`
- [ ] `numfmt`
- [ ] `od`
- [ ] `paste`
- [ ] `pathchk`
- [ ] `pinky`
- [ ] `pr`
- [ ] `printenv`
- [ ] `printf`
- [ ] `ptx`
- [ ] `pwd`
- [ ] `readlink`
- [ ] `realpath`
- [ ] `rm`
- [ ] `rmdir`
- [ ] `runcon`
- [ ] `seq`
- [ ] `sha1sum`
- [ ] `sha224sum`
- [ ] `sha256sum`
- [ ] `sha384sum`
- [ ] `sha512sum`
- [ ] `shred`
- [ ] `shuf`
- [ ] `sleep`
- [X] `sort`
- [ ] `split`
- [ ] `stat`
- [ ] `stdbuf`
- [ ] `stty`
- [ ] `sum`
- [ ] `sync`
- [ ] `tac`
- [ ] `tail`
- [ ] `tee`
- [ ] `test`
- [ ] `timeout`
- [ ] `touch`
- [ ] `tr`
- [ ] `true`
- [ ] `truncate`
- [ ] `tsort`
- [ ] `tty`
- [ ] `uname`
- [ ] `unexpand`
- [X] `uniq`
- [ ] `unlink`
- [ ] `users`
- [ ] `vdir`
- [ ] `wc`
- [ ] `who`
- [ ] `whoami`
- [ ] `yes`


/*
 * Copyright (c) 2021 Cameron Himes
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <queue>
#include <string>
#include <iostream>
#include <filesystem>

void crash(std::string error) {
    std::cerr << error << std::endl;
    exit(1);
}

std::string extractFileName(std::string path) {
    if (path.find('/') == path.npos) {
        return path;
    }
    else {
        int index = path.find_last_of('/');
        return path.substr(index, path.size());
    }
}

void copyHelper(std::string file_origin, std::string file_destination) {
    try {
        // perform copy operation
        std::filesystem::copy(file_origin.c_str(), file_destination.c_str());
    }
    catch (std::filesystem::__cxx11::filesystem_error e) {
        std::cerr << e.what() << std::endl;
    }
}

void copy(std::queue<std::string> file_queue, std::string file_destination) {
    // check if destination is a directory
    bool isDirectory = file_queue.size() > 1;
    if (isDirectory) {
        if (!std::filesystem::exists(file_destination)) {
            std::filesystem::create_directory(file_destination);
        }
    }
    // copy each file in the queue
    while (!file_queue.empty()) {
        // get the destination
        std::string dest = isDirectory ? file_destination + '/' + extractFileName(file_queue.front()) : file_destination;
        // perform copy operation
        copyHelper(file_queue.front(), dest);
        // remove from queue
        file_queue.pop();
    }
}

void check(std::queue<std::string> file_queue, std::string file_destination) {
    // check each item in the queue
    while (!file_queue.empty()) {
        if (!std::filesystem::exists(file_queue.front())) {
            crash("File or directory does not exist: " + file_queue.front());
        }
        file_queue.pop();
    }
}

int main(int argc, char **argv) {
    // determine mode of operation
    if (argc == 1) {
        // show error for not having any paths
        crash("Missing origin and destination");
    }
    else if (argc == 2) {
        // show error for not having a destination
        crash("Missing destination");
    }
    else {
        // load file paths into queue
        std::queue<std::string> file_queue;
        for (int i = 1; i < argc - 1; i++) {
            file_queue.push(argv[i]);
        }
        // grab the destination
        std::string file_destination = argv[argc - 1];
        // check if valid
        check(file_queue, file_destination);
        // call the copier
        copy(file_queue, file_destination);
    }
    // exit the program
    return 0;
}

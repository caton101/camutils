/*
 * Copyright (c) 2021 Cameron Himes
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string>
#include <iostream>
#include <unordered_set>
#include <argp.h>

#define MODE_SLOW 0
#define MODE_FAST 1

// define a function pointer
typedef bool (*fptr)(std::string);

// stores the mode
int mode = MODE_SLOW;

// description of the program
char doc[] = "uniq -- extract unique lines from a file";
// version of the program
const char *argp_program_version = "uniq 1.0";
// required but not being used
char args_doc[] = "";

//store the possible arguments
struct argp_option options[] = {
    {
        .name = NULL,
        .key = 'f',
        .flags = 0,
        .doc = "fast mode (only check for immediate repeats)",
        .group = 0
    },
    {
        .name = "fast",
        .key = 0,
        .flags = OPTION_ALIAS,
        .doc = "fast mode (only check for immediate repeats)",
        .group = 0
    },
    {
        .name = NULL,
        .key = 's',
        .flags = 0,
        .doc = "slow mode (check for all repeats)",
        .group = 0
    },
    {
        .name = "slow",
        .key = 0,
        .flags = OPTION_ALIAS,
        .doc = "slow mode (check for all repeats)",
        .group = 0
    },
    {
        .name = "FILE",
        .key = 0,
        .flags = 0,
        .doc = "input file",
        .group = 1
    },
    {0}
};

error_t parse_opt(int key, char *arg, struct argp_state *state) {
    // set run mode of argument is valid
    switch (key) {
        case 'f':
            // fast mode
            mode = MODE_FAST;
            break;
        case 's':
            // slow mode
            mode = MODE_SLOW;
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

// make the argument parser
struct argp argp = {options, parse_opt, args_doc, doc, NULL, NULL, NULL};

bool isUniqueFast(std::string str) {
    // make a container for the last used string
    static std::string lastUsed = "";
    // check if it already exists
    if (str == lastUsed) {
        // it is not unique
        return false;
    } else {
        // it is unique
        lastUsed = str;
        return true;
    }
}

bool isUniqueSlow(std::string str) {
    // make a container for the set
    static std::unordered_set<std::string> data;
    // check if string is inside the set
    if (data.find(str) == data.end()) {
        // it is NOT in the set
        data.insert(str);
        return true;
    } else {
        // it IS in the set
        return false;
    }
}

fptr setMode() {
    // return a pointer to the correct unique function
    switch (mode) {
        case MODE_SLOW:
            // use slow version
            return isUniqueSlow;
        case MODE_FAST:
            // use fast version
            return isUniqueFast;
        default:
            // use slow by default
            return isUniqueSlow; 
    }
}

void uniq() {
    // set the unique function
    fptr isUniqueFunction = setMode();
    // initialize the buffer
    std::string buffer;
    // keep reading until empty
    while (std::getline(std::cin, buffer)) {
        // check if unique
        if (isUniqueFunction(buffer)) {
            std::cout << buffer << std::endl;
        }
    }
}

int main(int argc, char ** argv) {
    // check arg count
    if (argc > 2) {
        std::cerr
            << "ERROR: More than one argument. The last one will be used."
            << std::endl;
    }
    // get the args
    argp_parse(&argp, argc, argv, 0, 0, NULL);
    // run uniq on STDIN
    uniq();
    // exit the program
    return 0;
}

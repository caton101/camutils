/*
 * Copyright (c) 2021 Cameron Himes
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>
#include <queue>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <unistd.h>

#define BUFFER_SIZE 16000 // 16 KB

void printFile(std::string path) {
    // make buffer
    static char buffer[BUFFER_SIZE];
    // make status flag
    static int s;
    // make a file object
    std::ifstream file;
    // open the file
    file.open(path, std::ios::binary | std::ios::ate);
    file.seekg(0, std::ios::beg);
    // fail if file failed to open
    if(!file)
    {
        write(STDERR_FILENO, "File ", 5);
        write(STDERR_FILENO, path.c_str(), path.size());
        write(STDERR_FILENO, " could not be opened for reading.", 34);
        exit(1);
    }
    // read the whole file
    while (!file.eof()) {
        file.read(buffer, BUFFER_SIZE);
        write(STDOUT_FILENO, &buffer, file.gcount());
    }
    // close the file
    file.close();
}

void printFiles(int argc, char ** argv) {
    // copy contents to a vector
    std::vector<std::string> filesVector;
    for (int i = 1; i < argc; i++) {
        filesVector.push_back(argv[i]);
    }
    // sort the data
    std::sort(filesVector.begin(), filesVector.end());
    
    // copy contents to a queue
    std::queue<std::string> filesQueue;
    for (auto itr = filesVector.begin(); itr != filesVector.end(); itr++) {
        filesQueue.push(*itr.base());
    }
    // destroy the vector
    filesVector.clear();
    // print all contents
    while (!filesQueue.empty()) {
        printFile(filesQueue.front());
        filesQueue.pop();
    }
}

void printSTDIN() {
    // make buffer
    static char buffer[BUFFER_SIZE];
    // make status flag
    static int s;
    // read until nothing is left
    while (true) {
        // read into buffer
        s = read(STDIN_FILENO, &buffer, BUFFER_SIZE);
        // check if finished
        if (!s) {
            break;
        }
        // write the buffer
        write(STDOUT_FILENO, &buffer, s);
    }
}

int main(int argc, char ** argv) {
    // determine mode of operation
    if (argc > 1) {
        // print from files
        printFiles(argc, argv);
    } else {
        // print from stdin
        printSTDIN();
    }
    // exit the program
    return 0;
}

/*
 * Copyright (c) 2021 Cameron Himes
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <unistd.h>

int size(char * string) {
    // make character counter
    int c = 0;
    // search for null terminator
    while (string[c] != '\0') {
        c++;
    }
    // return the count
    return c;
}

int main(int argc, char **argv) {
    // show all arguments
    for (int i = 1; i < argc; i++) {
        // write the argument
        write(STDOUT_FILENO, argv[i], size(argv[i]));
        // if this is not the last one, append a space
        if (i+1 != argc) {
            write(STDOUT_FILENO, " ", 1);
        }
    }
    // append newline
    write(STDOUT_FILENO, "\n", 1);
    // exit normally
    return 0;
}

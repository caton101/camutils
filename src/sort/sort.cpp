/*
 * Copyright (c) 2021 Cameron Himes
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

// save my poor hands from that word-heavy thing
#define SVEC std::vector<std::string>

SVEC read() {
    // make a buffer
    std::string buffer;
    // make a vector
    SVEC container;
    // keep reading until empty
    while(std::getline(std::cin, buffer)) {
        container.push_back(buffer);
    }
    // return the vector
    return container;
}

void print(SVEC lines) {
    for (std::string itr : lines) {
        printf("%s\n", itr.c_str());
    }
}

int main(int argc, char ** argv) {
    // get lines
    SVEC lines = read();
    // sort the lines
    std::sort(lines.begin(), lines.end());
    // print the lines
    print(lines);
    // exit the program
    return 0;
}

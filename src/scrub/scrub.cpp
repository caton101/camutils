/*
 * Copyright (c) 2021 Cameron Himes
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <unistd.h>
#include <argp.h>

#define ASCII_MIN 32  // SPACE
#define ASCII_MAX 126 // ~
#define ASCII_LF 10 // \n
#define ASCII_CR 13 // \r
#define ASCII_TAB 9 // \t
#define BUFFER_SIZE 16000 // 16 KB
#define MODE_GENERIC 0
#define MODE_ASCII 1

// define a function pointer
typedef bool (*fptr)(int);

// stores the filter mode
int mode = MODE_GENERIC;

// description of the program
char doc[] = "scrub -- a filter to extract printable text from any file format";
// version of the program
const char *argp_program_version = "scrub 2.0";
// required but not being used
char args_doc[] = "";

// stores the possible arguments
struct argp_option options[] = {
    {
        .name = NULL,
        .key = 'g',
        .arg = NULL,
        .flags = 0,
        .doc = "Only allow ASCII values in range [9,10,13,32-126]",
        .group = 0
    },
    {
        .name = "generic",
        .key = 0,
        .arg = NULL,
        .flags = OPTION_ALIAS,
        .doc = "Only allow ASCII values in range [9,10,13,32-126]",
        .group = 0
    },
    {
        .name = NULL,
        .key = 'a',
        .arg = NULL,
        .flags = 0,
        .doc = "Allow all standard ASCII characters",
        .group = 0
    },
    {
        .name = "ascii",
        .key = 0,
        .arg = NULL,
        .flags = OPTION_ALIAS,
        .doc = "Allow all standard ASCII characters",
        .group = 0
    },
    {0}
};

error_t parse_opt(int key, char *arg, struct argp_state *state) {
    // set run mode if the argument is valid
    switch (key)
    {
        case 'g':
            // generic mode
            mode = MODE_GENERIC;
            break;
        case 'a':
            // ascii mode
            mode = MODE_ASCII;
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

// make argument parser
struct argp argp = {options, parse_opt, args_doc, doc, NULL, NULL, NULL};

bool isValidGeneric(int character) {
    // check for normal characters
    if (character >= ASCII_MIN && character <= ASCII_MAX) {
        return true;
    }
    // check for control characters
    if (character == ASCII_LF
        || character == ASCII_CR
        || character == ASCII_TAB)
    {
        return true;
    }
    // it is not valid
    return false;
}

bool isValidASCII(int character) {
    // standard ASCII range is 0 - 127
    return character >= 0 && character <= 127;
}

fptr setFilter() {
    // return a pointer to that mode's filter
    switch (mode) {
        case MODE_GENERIC:
            // use the generic filter function
            return isValidGeneric;
        case MODE_ASCII:
            // use the ascii filter function
            return isValidASCII;
        default:
            // use the generic filter function
            return isValidGeneric;
    }
}

void filter() {
    // set the function to call based on the mode
    fptr validFunction = setFilter();
    // make input buffer
    char ib[BUFFER_SIZE];
    // make output buffer
    char ob[BUFFER_SIZE];
    // make status flag
    int s;
    // loop over stdin
    while (1) {
        // read buffer
        s = read(STDIN_FILENO, &ib, BUFFER_SIZE);
        // check for exit condition
        if (!s) {
            break;
        }
        // process characters
        int obp = 0;
        for (int i = 0; i < s; i++) {
            // copy if valid
            if (validFunction(ib[i])) {
                ob[obp] = ib[i];
                obp++;
            }
        }
        // output filtered buffer
        write(STDOUT_FILENO, &ob, obp);
    }
}

int main(int argc, char **argv) {
    // parse the arguments
    argp_parse(&argp, argc, argv, 0, 0, NULL);
    // run the filter
    filter();
    // exit program  
    return 0;
}
